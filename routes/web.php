<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('index');
Route::get('/account', 'FrontController@account')->name('account')->middleware('auth');
Route::post('/add-adress', 'FrontController@adress')->name('add_adress')->middleware('auth');
Route::get('/offres', 'FrontController@offers')->name('offers');
Route::get('/cart', 'FrontController@cart')->name('cart')->middleware('auth');
Route::post('/pay', 'FrontController@payment')->name('payment')->middleware("auth");

Route::get('/article/{id}-{slug}', 'ArticleController@show')->name('article.show');
Route::get('/article/nouveau', 'ArticleController@create')->name('article.create');
Route::post('/article/nouveau', 'ArticleController@store')->name('article.store');
Route::get('/categorie/{id}-{slug}', 'ArticleController@category')->name('article.category');
Route::get('/tag/{id}-{slug}', 'ArticleController@tag')->name('article.tag');

Route::get('/forum', 'ForumController@index')->name('forum.index');
Route::get('/forum/{id}-{slug}', 'ForumController@show')->name('forum.show');
Route::post('/forum/{id}-{slug}', 'ForumController@post')->name('forum.post');

Auth::routes();

