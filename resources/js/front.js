const subscriptionButtons = document.querySelectorAll("[data-subscription]");
const cartLink = document.getElementById("cart-link");
for (let i = 0; i < subscriptionButtons.length; i++) {
    const subscriptionButton = subscriptionButtons[i];
    subscriptionButton.addEventListener("click", () => {
        var d = new Date();
        d.setTime(d.getTime() + (5 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "subscription=" + subscriptionButton.getAttribute("data-subscription") + ";" + expires + ";path=/";
        cartLink.classList.add("active");
        setTimeout(() => {
                cartLink.classList.remove("active")
            },
            1000)
    })
}
document.getElementById("categories-toggle").addEventListener("click", () => {
    document.getElementById("category-list").classList.toggle('active')
});
