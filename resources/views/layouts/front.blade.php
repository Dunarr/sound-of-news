<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/front.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ mix('css/front.css') }}" rel="stylesheet">
    @stack("styles")
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">

            <ul>
                <li><a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a></li>
                <li><a id="categories-toggle" class="navbar-brand">
                        Catégories
                    </a></li>
                <li><a href="{{ route('forum.index') }}">Forum</a></li>
                @if (Auth::check())
                    <li><a href="{{ route('account') }}">Mon compte</a></li>
                @else
                    <li><a href="{{ route('login') }}">Connexion</a></li>
                    <li><a href="{{ route('register') }}">Inscription</a></li>
                @endif
                @isNotPremium
                <li><a href="{{ route('offers') }}">Abonnez vous</a></li>
                <li><a id="cart-link" href="{{ route('cart') }}">panier</a></li>

                @endif
                @hasRight("access_crud")
                <li><a href="/admin/articles">Admin</a></li>
                @endif
            </ul>
        </div>
        <ul id="category-list">
            @foreach($categories as $category)
                <li><a id="cart-link" href="{{ route('article.category',["id" => $category->id,  "slug" => $category->slug]) }}">{{$category}}</a></li>
            @endforeach
        </ul>
    </nav>


    <main class="py-4">
        @if(Session::has('success'))
            <div class="flash-success">{{Session::get('success')}}</div>
            @php Session::remove('success') @endphp
        @endif()
        @if(Session::has('error'))
            <div class="flash-success">{{Session::get('error')}}</div>
            @php Session::remove('error') @endphp
        @endif()
        @yield('content')
    </main>
</div>
</body>
@stack("scripts")
</html>
