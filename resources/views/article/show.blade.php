@extends('layouts.front')
@section('content')
    <h1>{{$article->title}}</h1>
    <br>
    @if($article->premium)
        @isPremium
        <p>{{$article->content}}</p>
        <div>
            <h3>Tags:</h3>
            @foreach($article->tags as $tag)
                <a class="tag-pill" href="{{ route('article.tag',["id" => $tag->id,  "slug" => $tag->slug]) }}">{{$tag->title}}</a>
            @endforeach
        </div>
        @else
            <a href="{{ route('offers') }}">Abonez-vous pour visualiser cet article</a>
        @endif
    @else
        <p>{{$article->content}}</p>
        <div>
            <h3>Tags:</h3>
            @foreach($article->tags as $tag)
                <a class="tag-pill" href="{{ route('article.tag',["id" => $tag->id,  "slug" => $tag->slug]) }}">{{$tag->title}}</a>
            @endforeach
        </div>

    @endif
@endsection
