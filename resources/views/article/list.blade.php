@extends('layouts.front')
@section('content')
    <h1>{{ $title }}</h1>
    @foreach($articles as $article)
       @include('article.teaser')
    @endforeach
    {{ $articles->links() }}
@endsection
