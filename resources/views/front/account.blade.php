@extends('layouts.front')
@section('content')
    <h1>Mon compte: {{$user->name}}</h1>
    <form action="{{ route('logout') }}" method="POST">@csrf <input type="submit" value="Me déconnecter"></form>
    @isPremium()
    <p>Vous êtes abboné premium</p>
    @else
        <p>vous n'êtes pas abonné, <a href="{{ route('offers') }}">Abonez-vous pour consulter tout notre contenu</a></p>
    @endif


    @if(sizeof($user->invoiceAdresses))
        <h2>Vos adresses de facturation:</h2>
        <ul>
            @foreach($user->invoiceAdresses as $adress)
                <li>{{$adress}}</li>
            @endforeach
        </ul>

    @endif
    <h2>Ajoutez une adresse</h2>
    <form action="{{ route('add_adress') }}" method="POST">
        @csrf()
        <div>
            <label for="adress">Adresse (ligne 1)</label> <br>
            <input type="text" id="adress" name="adress">
        </div>

        <div>
            <label for="adress2">Adresse (ligne 2)</label> <br>
            <input type="text" id="adress2" name="adress2">
        </div>

        <div>
            <label for="zip_code">Code postal</label> <br>
            <input type="text" id="zip_code" name="zip_code">
        </div>

        <div>
            <label for="city">Ville</label> <br>
            <input type="text" id="city" name="city">
        </div>

        <div>
            <label for="country">pays</label> <br>
            <input type="text" id="country" name="country">
        </div>
        <input type="submit">
    </form>
@endsection
