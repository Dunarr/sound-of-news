@extends('layouts.front')
@section('content')
    <h1>Panier</h1>
   Abonnement: {{ $subscription->duration }}<br>
    Prix de l'abonnement: {{$subscription->price}}€<br><br>
    @if(sizeof($adresses))
    <form action="{{ route('payment') }}" method="POST">
        @csrf
        <select name="adress">
            @foreach($adresses as $adress)
                <option value="{{$adress->id}}">{{$adress}}</option>
            @endforeach
        </select>
        <input type="hidden" name="subscription" value="{{$subscription->id}}">
        <input type="submit" value="Payer">
    </form>
    @else
        <p>Vous n'avez pas d'adresses dans votre compte, merci d'en renseigner une pour continuer. <br>
            <a href="{{ route('account') }}">Mon compte</a></p>
    @endif
@endsection
