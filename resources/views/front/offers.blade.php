@extends('layouts.front')
@section('content')
    @foreach($subscriptions as $subscription)
       <div data-subscription="{{$subscription->id}}"><h3>{{$subscription->duration}}</h3> for {{$subscription->price}}€</div> <br>
    @endforeach
@endsection
