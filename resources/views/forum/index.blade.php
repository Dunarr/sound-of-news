@extends('layouts.front')
@section('content')
    <h1>Forum</h1>
    <table>
        @foreach($threads as $thread)
        <tr>
            <td><a href="{{ route('forum.show',["id" => $thread->id,  "slug" => $thread->slug]) }}">{{$thread->title}}</a></td>
            <td>{{sizeof($thread->messages)}}</td>
        </tr>
        @endforeach
    </table>
    {{ $threads->links() }}

@endsection
