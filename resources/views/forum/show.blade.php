@extends('layouts.front')
@section('content')
    <h1>{{$thread->title}}</h1>
    <table>
        @foreach($thread->messages as $message)
        <tr>
            <td>{{$message->user}}</td>
            <td>{{$message->content}}</td>
        </tr>
        @endforeach
    </table>
@isPremium
    <form action="" method="POST">
        @csrf
        <label for="content">Message</label>
        <textarea style="width: 100%" name="content" id="content" cols="30" rows="10"></textarea>
        <input type="submit">
    </form>
    @else
    <a href="{{ route('offers') }}">Abonez-vous pour Participer sur les forums</a>
    @endif
@endsection
