<?php

namespace App\Providers;

use Auth;
use Illuminate\Support\ServiceProvider;
use Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('hasRight', function ($right) {
            return "<?php if(Auth::user() && Auth::user()->hasRight($right)): ?>";
        });
        Blade::directive('isPremium', function ($right) {
            return "<?php if(Auth::user() && Auth::user()->isPremium()): ?>";
        });
        Blade::directive('isNotPremium', function ($right) {
            return "<?php if(!Auth::user() || !Auth::user()->isPremium()): ?>";
        });
    }
}
