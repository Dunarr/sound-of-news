<?php

namespace App\Providers;

use App\Listeners\MCIndexChanger;
use App\Listeners\MCUpdateChanger;
use App\Listeners\RegisterVerif;
use Dunarr\MagiCrud\Events\MagiCrudDisplayIndex;
use Dunarr\MagiCrud\Events\MagiCrudUpdateEntity;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            RegisterVerif::class
        ],
        'Illuminate\Auth\Events\Registered' => [
            'App\Listeners\RegisterVerif'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
