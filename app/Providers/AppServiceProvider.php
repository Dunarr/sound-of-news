<?php

namespace App\Providers;

use Dunarr\MagiCrud\Composers\MenuComposer;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composers([
            \App\Composers\MenuComposer::class => ['layouts.front']
        ]);
    }
}
