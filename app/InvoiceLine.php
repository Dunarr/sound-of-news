<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\InvoiceLine
 *
 * @property int $id
 * @property float $price
 * @property string $item
 * @property int $quantity
 * @property int|null $invoice_id
 * @property int|null $subscription_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Invoice|null $invoice
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine whereItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceLine whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class InvoiceLine extends Model
{
    protected $fillable = ['price','item','quantity'];
    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }
    public function setSubscriptionAttribute($subscription)
    {
        dd($subscription);
    }

    public function __toString()
    {
        return $this->item.' '.$this->quantity;
    }
}
