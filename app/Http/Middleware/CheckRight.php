<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRight
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string $right
     * @return mixed
     */
    public function handle($request, Closure $next, $rights)
    {
        if (!auth()->check()) {
            return redirect()->route('login');
        }
        $rights = explode("|", $rights);
        foreach ($rights as $right) {
            if (auth()->user()->hasRight($right)) {
                return $next($request);
            }
        }
        abort('403');
    }
}
