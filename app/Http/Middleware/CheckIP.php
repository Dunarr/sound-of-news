<?php

namespace App\Http\Middleware;

use Closure;

class CheckIP
{
    /**
     * List of authorized IPs
     *
     * @var array $authorizedIPs
     */
    protected $authorizedIPs = ['127.0.0.1'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(in_array($request->getClientIp(), $this->authorizedIPs)){
            return $next($request);
        }
        return abort('403');
    }
}
