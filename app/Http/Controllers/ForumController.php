<?php

namespace App\Http\Controllers;

use App\Message;
use App\Thread;
use Illuminate\Http\Request;

class ForumController extends Controller
{

    public function index()
    {
        $threads = Thread::paginate(20);
        return view("forum.index", compact('threads'));
    }

    public function show($id, $slug)
    {
        $thread = Thread::findOrFail($id);
        if ($slug != $thread->slug) {
            return response()->redirectToRoute('forum.show', ['id' => $thread->id, 'slug' => $thread->slug], 301);
        }
        $thread->load('messages');
        return view('forum.show', compact('thread'));
    }

    public function post(Request $request, $id)
    {
        $thread = Thread::findOrFail($id);
        $message = Message::create($request->all());
        $message->thread()->associate($thread);
        $message->user()->associate(\Auth::user());
        $message->save();
        return response()->redirectToRoute('forum.show', ['id' => $thread->id, 'slug' => $thread->slug], 301);
    }
}
