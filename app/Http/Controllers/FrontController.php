<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\InvoiceAdress;
use App\InvoiceLine;
use App\Subscription;
use App\User;
use Auth;
use App\Article;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        $articles = Article::take(5)->get();
        $title = 'Nos derniers articles';

        return view("front.home", compact('articles', "title"));
    }

    public function account()
    {
        /** @var User $user */
        $user = Auth::user();
        $user->load(['invoices', 'subscription', 'invoiceAdresses']);

        return view("front.account", compact('user'));
    }

    public function invoice(Invoice $invoice)
    {
        /** @var User $user */
        $user = Auth::user();
        if($user->id != $invoice->user->id)
            abort('403');
        return view("front.invoice", compact('invoice'));
    }

    public function offers()
    {
        $subscriptions = Subscription::all();
        return view("front.offers", compact('subscriptions'));
    }

    public function adress(Request $request)
    {
        $adress = InvoiceAdress::create($request->all());
        $adress->user()->associate(Auth::user());
        $adress->save();
        return redirect()->route('account');
    }

    public function cart(Request $request)
    {
        $subscriptionId = $request->cookie('subscription');
        if (!$subscriptionId) {
            $request->session()->flash('error', 'Veuillez séléctionner un abonnement');
            return redirect()->route('offers');
        }
        $subscription = Subscription::find($subscriptionId);
        $adresses = Auth::user()->invoiceAdresses;
        return view("front.cart", compact('subscription', 'adresses'));
    }

    public function payment(Request $request)
    {
        $user = Auth::user();
        if ($end = $user->isPremium()) {
            $request->session()->flash('error','Vous êtes déja abonné jusqu\'au '.$end);
            return redirect()->route('cart');
        }
        $subscription = null;
        $subscriptionId = $request->input('subscription');
        $adressId = $request->input('adress');
        if (!$subscriptionId) {
            $request->session()->flash('error','Aucun abbonement séléctionné');
            return redirect()->route('cart');
        }
        if (!$adressId) {
            $request->session()->flash('error', 'Aucune adresse choisie');
            return redirect()->route('cart');
        }
        $subscription = Subscription::find($subscriptionId);
        $adress = InvoiceAdress::find($adressId);
        if($adress->user->id != Auth::user()->id){
            $request->session()->flash('error', 'Choix invalide détécté');
            return redirect()->route('cart');
        }
        /** @var User $user */
        $user = Auth::user();
        $user->subscription()->associate($subscription);
        $user->save();
        $invoice = new Invoice();
        $invoice->number = rand(100000000, 999999999);
        $invoice->status = 'VALIDATED';
        $invoice->total_price = $subscription->price;
        $invoice->total_quantity = 1;
        $invoice->invoiceAdresse()->associate($adress);
        $invoice->user()->associate($user);
        $invoice->save();
        $invoiceLine = new InvoiceLine();
        $invoiceLine->item = $subscription->duration . ' - (' . $subscription->price . ')';
        $invoiceLine->quantity = 1;
        $invoiceLine->price = $subscription->price;
        $invoiceLine->invoice()->associate($invoice);
        $invoiceLine->subscription()->associate($subscription);
        $invoiceLine->save();
        $request->session()->flash('success', 'Abonnement enregistré');
        return redirect()->route('index');
    }
}
