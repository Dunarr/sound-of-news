<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Tag;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ArticleController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        Article::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @param $slug
     * @return Factory|RedirectResponse|View
     */
    public function show($id, $slug)
    {
        $article = Article::findOrFail($id);
        if ($slug != $article->slug) {
            return response()->redirectToRoute('article.show', ['id' => $article->id, 'slug' => $article->slug], 301);
        }
        return view('article.show', compact('article'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @param $slug
     * @return Factory|View|RedirectResponse
     */
    public function category($id, $slug)
    {
        $category = Category::findOrFail($id);
        if ($slug != $category->slug) {
            return response()->redirectToRoute('article.category', ['id' => $category->id, 'slug' => $category->slug], 301);
        }
        $articles = $category->articles()->paginate(10);
        $title = 'Catégorie : "'.$category->title.'"';

        return view("article.list", compact('articles', "title"));
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @param $slug
     * @return Factory|View|RedirectResponse
     */
    public function tag($id, $slug)
    {
        $tag = Tag::findOrFail($id);
        if ($slug != $tag->slug) {
            return response()->redirectToRoute('article.tag', ['id' => $tag->id, 'slug' => $tag->slug], 301);
        }
        $articles = $tag->articles()->paginate(10);
        $title = 'Les articles liés au tag: "'.$tag->title.'"';

        return view("article.list", compact('articles', "title"));
    }
}
