<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [
            'title.required' => "Titre requis",
            'title.min' => "Le titre est trop court",
            'content.required' => "le contenu est requis",
            'content.min' => "le contenu est trop court",
            'category_id' => "la categorie est requise",
        ];
    }

}
