<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Medium
 *
 * @property int $id
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Article[] $articles
 * @property-read int|null $articles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Medium newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Medium newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Medium query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Medium whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Medium whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Medium wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Medium whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Medium extends Model
{
    protected $fillable = ['path'];
    public function articles(){
        return $this->hasMany(Article::class);
    }
}
