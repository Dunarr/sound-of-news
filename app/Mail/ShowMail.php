<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShowMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $article;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $article)
    {
        $this->user = $user;
        $this->article = $article;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contact@dunarr.com')->to($this->user)->view('mail.showMail', ['user'=>$this->user, 'article'=>$this->article])->attach(public_path().'/img/git_logo.png');
    }
}
