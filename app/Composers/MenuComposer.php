<?php namespace App\Composers;

use App\Category;

class MenuComposer
{
    public function compose($view)
    {
        //Add your variables
        $view->with('categories',
            Category::all()
        );
    }
}
