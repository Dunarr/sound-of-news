<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $defaultCat = new \App\Category();
        $defaultCat->title = 'without category';
        $defaultCat->save();


        $rights = ["always_premium", "access_crud"];
        $rightEntities = [];
        foreach ($rights as $rightVal){
            $right = new \App\Right();
            $right->name = $rightVal;
            $right->save();
            array_push($rightEntities, $right->id);
        }

        $roleAdmin = new \App\Role();
        $roleAdmin->title = 'Administrator';
        $roleAdmin->save();
        $roleAdmin->rights()->sync($rightEntities);
        $roleAdmin->save();

        $roleAuthor = new \App\Role();
        $roleAuthor->title = 'Author';
        $roleAuthor->save();

        $roleUser = new \App\Role();
        $roleUser->title = 'User';
        $roleUser->save();

        $admin = new \App\User();
        $admin->role()->associate($roleAdmin);
        $admin->name = 'admin';
        $admin->email = 'william@dunarr.com';
        $admin->plainPassword = 'admin';
        $admin->renew = false;
        $admin->token = false;
        $admin->active = true;
        $admin->save();
    }
}
