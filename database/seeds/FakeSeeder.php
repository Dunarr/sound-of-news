<?php

use App\Subscription;
use App\User;
use Illuminate\Database\Seeder;

class FakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Subscription::class, 3)->create();
        factory(App\Category::class, 10)->create();
        factory(App\User::class, 10)->create();

        factory(App\Thread::class, 10)->make()->each(function (\App\Thread $thread) {
            $thread->user()->associate(App\User::inRandomOrder('')->first())->save();
        });
        factory(App\Tag::class, 20)->make()->each(function (App\Tag $tag) {
            $tag->user()->associate(App\User::inRandomOrder('')->first())->save();
        });
        factory(App\Article::class, 50)->make()->each(function (App\Article $article) {
            $article->category()->associate(App\Category::inRandomOrder('')->first());
            $article->user()->associate(App\User::inRandomOrder('')->first())->save();
            $article->tags()->sync(App\Tag::inRandomOrder('')->take(10)->get());
            $article->save();
        });
        factory(App\Comment::class, 10)->make()->each(function (App\Comment $comment) {
            $comment->user()->associate(App\User::inRandomOrder('')->first());
            $comment->article()->associate(App\Article::inRandomOrder('')->first());
            $comment->save();
        });
        factory(App\Message::class, 10)->make()->each(function (App\Message $message) {
            $message->user()->associate(App\User::inRandomOrder('')->first())->save();
            $message->thread()->associate(App\Thread::inRandomOrder('')->first())->save();
        });
        factory(App\Medium::class, 10)->create();
        factory(App\Report::class, 10)->make()->each(function (App\Report $report) {
            $report->content()->associate(App\Article::inRandomOrder('')->first())->save();
        });
        factory(App\InvoiceAdress::class, 10)->make()->each(function (App\InvoiceAdress $invoiceAdress) {
            $invoiceAdress->user()->associate(App\User::inRandomOrder('')->first())->save();
        });
        factory(App\Invoice::class, 10)->make()->each(function (App\Invoice $invoice) {
            /** @var User $user */
            $user = App\User::inRandomOrder('')->first();
            $invoice->user()->associate($user)->save();
            $invoice->invoiceAdresse()->associate($user->invoiceAdresses()->inRandomOrder('')->first())->save();
        });

        factory(App\InvoiceLine::class, 10)->make()->each(function (App\InvoiceLine $invoiceLine) {
            /** @var Subscription $subscription */
            $subscription = App\Subscription::inRandomOrder('')->first();
            $invoiceLine->price = $subscription->price;
            $invoiceLine->item = $subscription->duration . ' - (' . $subscription->price . ')';
            $invoiceLine->quantity = 1;
            $invoiceLine->subscription()->associate($subscription)->save();
            $invoiceLine->invoice()->associate(App\Invoice::inRandomOrder('')->first())->save();
        });

    }
}
