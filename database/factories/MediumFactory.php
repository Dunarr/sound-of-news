<?php

/** @var Factory $factory */

use App\Medium;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Medium::class, function (Faker $faker) {
    return [
        "path" => $faker->imageUrl()
    ];
});
