<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Tag::class, function (Faker $faker) {
    return [
        'title' => $title = $faker->realText($faker->numberBetween($min = 15, $max = 50)),
        'slug' => Str::slug($title)
    ];
});
