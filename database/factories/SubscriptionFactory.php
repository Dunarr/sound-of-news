<?php

/** @var Factory $factory */

use App\Subscription;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        "price"=>$faker->randomFloat(2, 10, 100),
        "duration"=>$faker->randomElement(['1 year', '2 years', '1 month', '1 week', '6 months', '3 months']),
    ];
});
