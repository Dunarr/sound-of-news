<?php

/** @var Factory $factory */

use App\Article;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->realText($faker->numberBetween($min = 15, $max = 50)),
        'content' => $faker->realText(1000),
        'premium' => $faker->boolean(25),
        'published' => $faker->boolean(75),
    ];
});
