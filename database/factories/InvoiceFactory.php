<?php

/** @var Factory $factory */

use App\Invoice;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        "number"=>$faker->numberBetween(100000, 999999),
        "status"=>$faker->randomElement(['VALIDATED', 'PROCESSING', 'CANCELED', 'FAILED']),
    ];
});
