<?php

/** @var Factory $factory */

use App\InvoiceAdress;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(InvoiceAdress::class, function (Faker $faker) {
    return [
        "adress"=>$faker->address,
        "city"=>$faker->city,
        "zip_code"=>$faker->postcode,
        "country"=>$faker->country,
        "adress2"=>$faker->secondaryAddress,
    ];
});
